package life.weaken.lobby.cache;

import life.weaken.core.utils.FutureUtils;
import life.weaken.lobby.LobbyPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

public class ServerDataCache {

    private final BukkitTask updateTask;
    private final Map<String, ServerData> dataMap;

    public ServerDataCache(LobbyPlugin plugin) {
        dataMap = new HashMap<>();
        updateTask = new BukkitRunnable() {
            @Override
            public void run() {
                for(String server : plugin.getServerList()) {
                    Future<Integer> playersFuture = plugin.getCore().getBungeeAPI().getPlayerCount(server);
                    Future<Integer> maxPlayersFuture = plugin.getCore().getBungeeAPI().getMaxPlayers(server);
                    Future<String> statusFuture = plugin.getCore().getBungeeAPI().getServerStatus(server);

                    FutureUtils.waitForFutures(plugin, () -> dataMap.put(server, new ServerData(FutureUtils.get(playersFuture),
                            FutureUtils.get(maxPlayersFuture),
                            FutureUtils.get(statusFuture))
                    ), playersFuture, maxPlayersFuture, statusFuture);
                }
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 20L);

    }

    public void shutdown() {
        updateTask.cancel();
    }

    private ServerData getData(String server) {
        ServerData data = dataMap.get(server);
        if(data == null) return new ServerData(0, 0, "OFFLINE");
        return data;
    }

    public Integer getOnline(String server) {
        return getData(server).getOnline();
    }

    public Integer getMaxOnline(String server) {
        return getData(server).getMaxOnline();
    }

    public String getStatus(String server) {
        String status = getData(server).getStatus();
        if(status.equalsIgnoreCase("ONLINE")) return "&aOnline";
        if(status.equalsIgnoreCase("WHITELISTED")) return "&eWhitelisted";
        return "&cOffline";
    }

}
