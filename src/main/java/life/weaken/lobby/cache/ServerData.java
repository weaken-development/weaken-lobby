package life.weaken.lobby.cache;

public record ServerData(int online, int maxOnline, String status) {

    public int getOnline() {
        return online;
    }

    public int getMaxOnline() {
        return maxOnline;
    }

    public String getStatus() {
        return status;
    }
}
