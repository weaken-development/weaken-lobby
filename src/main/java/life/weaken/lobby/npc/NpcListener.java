package life.weaken.lobby.npc;

import life.weaken.lobby.LobbyPlugin;
import net.citizensnpcs.api.event.NPCClickEvent;
import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NpcListener implements Listener {

    private final LobbyPlugin plugin;

    public NpcListener(LobbyPlugin plugin) {
        super();
        this.plugin = plugin;
    }

    @EventHandler
    public void onNPCRightClick(NPCRightClickEvent event) {
        onNPCClick(event);
    }

    @EventHandler
    public void onNPCLeftClick(NPCLeftClickEvent event) {
        onNPCClick(event);
    }

    public void onNPCClick(NPCClickEvent event) {
        String server = plugin.getNpcManager().getServer(event.getNPC());
        if(server == null) return;
        event.getClicker().sendMessage(ChatColor.GREEN + "Connecting you to " + server);
        plugin.getCore().getBungeeAPI().connect(event.getClicker(), server);
    }

}
