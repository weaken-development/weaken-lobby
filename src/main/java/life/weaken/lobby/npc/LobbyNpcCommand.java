package life.weaken.lobby.npc;

import life.weaken.lobby.LobbyPlugin;
import life.weaken.lobby.npc.NpcManager;
import life.weaken.lobby.npc.storage.NpcStorage;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record LobbyNpcCommand(LobbyPlugin plugin) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "Only players can use this command");
            return true;
        }

        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <server> <skin|location|format>");
            return true;
        }

        String server = null;
        for (String s : plugin.getServerList()) if (s.equalsIgnoreCase(args[0])) server = s;
        if (server == null) {
            sender.sendMessage(ChatColor.RED + "Invalid server, available servers incluide: " + String.join(", ", plugin.getServerList()));
            return true;
        }

        NpcManager npcManager = plugin.getNpcManager();
        NpcStorage npcStorage = npcManager.getStorage();

        if (args[1].equalsIgnoreCase("toggle")) {
            boolean newValue = !npcStorage.getEnabled(server);
            npcStorage.setEnabled(server, newValue);
            npcManager.updateNpc(server);
            if (newValue) {
                sender.sendMessage(ChatColor.GREEN + "Enabled npc for " + server);
            } else {
                sender.sendMessage(ChatColor.GREEN + "Disabled npc for " + server);
            }
            return true;
        }

        if (!npcStorage.getEnabled(server)) {
            sender.sendMessage(ChatColor.RED + "This npc is currently disabled, you may enable it using /" + label + " " + server + " toggle");
            return true;
        }

        if (args[1].equalsIgnoreCase("location")) {
            Location location = player.getLocation().clone();
            location.setX(Math.floor(location.getX()) + 0.5);
            location.setZ(Math.floor(location.getZ()) + 0.5);

            npcStorage.setLocation(server, location);
            npcManager.respawnNpc(server);
            sender.sendMessage(ChatColor.GREEN + "Set npc location of " + server + " to " +
                    location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ());
            return true;
        } else if (args[1].equalsIgnoreCase("skin")) {
            if (args.length != 3) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <server> skin <uuid>");
                return true;
            }

            npcStorage.setSkin(server, args[2]);
            npcManager.updateNpc(server);
            sender.sendMessage(ChatColor.GREEN + "Set skin to " + args[2]);
            return true;
        } else if (args[1].equalsIgnoreCase("format")) {
            if (args.length < 4 || !Arrays.asList("top", "bottom").contains(args[2].toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <server> format <top|bottom> <format>");
                return true;
            }

            String format = String.join(" ", Arrays.stream(args).toList().subList(3, args.length));

            if (args[2].equalsIgnoreCase("top")) {
                npcStorage.setTopFormat(server, format);
                sender.sendMessage(ChatColor.GREEN + "Set top format to " + format);
                return true;
            } else if (args[2].equalsIgnoreCase("bottom")) {
                npcStorage.setBottomFormat(server, format);
                sender.sendMessage(ChatColor.GREEN + "Set bottom format to " + format);
                return true;
            }
        }
        sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <server> <skin|location|format>");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return plugin.getServerList().stream()
                    .filter(server -> server.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        } else if (args.length == 2) {
            return Stream.of("skin", "location", "format", "toggle")
                    .filter(c -> c.toLowerCase().startsWith(args[1].toLowerCase()))
                    .collect(Collectors.toList());
        } else if (args.length == 3 && args[1].equalsIgnoreCase("format")) {
            return Stream.of("top", "bottom")
                    .filter(c -> c.toLowerCase().startsWith(args[2].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
