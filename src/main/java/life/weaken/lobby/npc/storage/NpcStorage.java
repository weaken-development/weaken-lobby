package life.weaken.lobby.npc.storage;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class NpcStorage {

    private final YamlConfiguration config;
    private final File file;
    private final Map<String, NpcData> dataMap;

    private final Plugin plugin;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public NpcStorage(Plugin plugin) {
        this.plugin = plugin;
        file = new File(plugin.getDataFolder(), "data.yml");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch(Exception e) {
                plugin.getLogger().log(Level.SEVERE, "Failed to create data file:");
                e.printStackTrace();
            }
        }
        config = YamlConfiguration.loadConfiguration(file);
        dataMap = new HashMap<>();
    }

    public void load(String name) {
        Location loc = config.getLocation(name + ".location");
        String topFormat = config.getString(name + ".name.top");
        String bottomFormat = config.getString(name + ".name.bottom");
        String skin = config.getString(name + ".skin");
        boolean enabled = config.getBoolean(name + ".enabled");

        if(topFormat == null) topFormat = "Default server name!";
        if(bottomFormat == null) bottomFormat = "{ONLINE}/{MAX}";
        if(skin == null) skin = "latestLOG";

        dataMap.put(name, new NpcData(loc, topFormat, bottomFormat, skin, enabled));
    }

    @SuppressWarnings("all")
    public void save(String name, boolean save) {
        NpcData data = dataMap.get(name);
        if(data == null) return;
        Location location = data.getLocation();

        config.set(name + ".location", data.getLocation());
        config.set(name + ".name.top", data.getTopFormat());
        config.set(name + ".name.bottom", data.getBottomFormat());
        config.set(name + ".skin", data.getSkin());
        config.set(name + ".enabled", data.isEnabled());

        if(save) saveFile();
    }

    private void saveFile() {
        try {
            config.save(file);
        } catch(Exception e) {
            plugin.getLogger().log(Level.SEVERE, "Failed to save data file:");
            e.printStackTrace();
        }
    }

    public void saveAll() {
        for(String key : dataMap.keySet()) save(key, false);
        saveFile();
    }

    public NpcData getData(String name) {
        if(!dataMap.containsKey(name)) load(name);
        return dataMap.get(name);
    }

    public void setData(String name, NpcData data) {
        dataMap.put(name, data);
    }

    public Location getLocation(String server) {
        return getData(server).getLocation();
    }

    public String getTopFormat(String server) {
        return getData(server).getTopFormat();
    }

    public String getBottomFormat(String server) {
        return getData(server).getBottomFormat();
    }

    public String getSkin(String server) {
        return getData(server).getSkin();
    }

    public void setLocation(String server, Location location) {
        getData(server).setLocation(location);
    }

    public void setTopFormat(String server, String format) {
        getData(server).setTopFormat(format);
    }

    public void setBottomFormat(String server, String format) {
        getData(server).setBottomFormat(format);
    }

    public void setSkin(String server, String skin) {
        getData(server).setSkin(skin);
    }

    public boolean getEnabled(String server) {
        return getData(server).isEnabled();
    }

    public void setEnabled(String server, boolean value) {
        getData(server).setEnabled(value);
    }
}
