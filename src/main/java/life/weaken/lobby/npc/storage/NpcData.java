package life.weaken.lobby.npc.storage;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class NpcData {

    private Location location;
    private String topFormat;
    private String bottomFormat;
    private String skin;

    private boolean enabled;

    public NpcData(Location location, String topFormat, String bottomFormat, String skin, boolean enabled) {
        this.location = location;
        this.topFormat = topFormat;
        this.bottomFormat = bottomFormat;
        this.skin = skin;
        this.enabled = enabled;
    }

    public Location getLocation() {
        return location == null ? new Location(Bukkit.getWorld("world"), 0, 0, 0): location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getBottomFormat() {
        return bottomFormat;
    }

    public void setBottomFormat(String bottomFormat) {
        this.bottomFormat = bottomFormat;
    }

    public String getTopFormat() {
        return topFormat;
    }

    public void setTopFormat(String topFormat) {
        this.topFormat = topFormat;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
