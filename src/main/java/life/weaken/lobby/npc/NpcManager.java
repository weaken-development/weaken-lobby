package life.weaken.lobby.npc;

import life.weaken.lobby.LobbyPlugin;
import life.weaken.lobby.npc.storage.NpcData;
import life.weaken.lobby.npc.storage.NpcStorage;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.DespawnReason;
import net.citizensnpcs.api.npc.MemoryNPCDataStore;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import net.citizensnpcs.trait.HologramTrait;
import net.citizensnpcs.trait.SkinTrait;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;

import java.util.HashMap;
import java.util.Map;

public class NpcManager {

    private final Map<String, NPC> npcMap;
    private final NpcStorage npcStorage;

    private final LobbyPlugin plugin;

    private NPCRegistry customRegistry;

    public NpcManager(LobbyPlugin plugin) {
        npcMap = new HashMap<>();
        npcStorage = new NpcStorage(plugin);
        this.plugin = plugin;

        customRegistry = CitizensAPI.createAnonymousNPCRegistry(new MemoryNPCDataStore());
        for(String server : plugin.getServerList()) {
            if(!npcStorage.getEnabled(server)) continue;
            npcMap.put(server, createNpc(server));
        }

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for(String server : npcMap.keySet()) {
                NPC npc = npcMap.get(server);
                npc.getOrAddTrait(HologramTrait.class).setLine(0, formattedNpcName(server, npcStorage.getData(server).getTopFormat()));
                npc.setName(formattedNpcName(server, npcStorage.getData(server).getBottomFormat()));
            }
        }, 0L, 20L);

    }

    private NPC createNpc(String server) {
        NPC npc = customRegistry.createNPC(EntityType.PLAYER, server);

        NpcData data = npcStorage.getData(server);
        npc.getOrAddTrait(SkinTrait.class).setSkinName(data.getSkin(), true);
        npc.setAlwaysUseNameHologram(true);
        npc.spawn(data.getLocation());
        return npc;
    }

    public String getServer(NPC npc) {
        for(String key : npcMap.keySet()) if(npcMap.get(key) == npc) return key;
        return null;
    }

    public void updateNpc(String server) {
        NPC npc = npcMap.get(server);
        if(npcStorage.getEnabled(server)) {
            if(npc == null) {
                npc = createNpc(server);
                npcMap.put(server, npc);
                return;
            }
            NpcData data = npcStorage.getData(server);
            npc.getOrAddTrait(SkinTrait.class).setSkinName(data.getSkin());
            npc.setAlwaysUseNameHologram(true);
            respawnNpc(server);
        } else {
            if(npc != null) {
                npcMap.remove(server);
                npc.despawn(DespawnReason.REMOVAL);
                npc.destroy();
            }
        }

    }

    public void respawnNpc(String server) {
        NPC npc = npcMap.get(server);
        if(npc == null) return;
        npc.despawn(DespawnReason.PENDING_RESPAWN);
        npc.spawn(npcStorage.getLocation(server));
    }

    public void shutdown() {
        npcStorage.saveAll();
    }

    public NpcStorage getStorage() {
        return npcStorage;
    }

    public String formattedNpcName(String server, String format) {
        return ChatColor.translateAlternateColorCodes('&', format
                .replace("{ONLINE}", "" + plugin.getDataCache().getOnline(server))
                .replace("{MAX}", "" + plugin.getDataCache().getMaxOnline(server))
                .replace("{STATUS}", plugin.getDataCache().getStatus(server)));
    }

}
