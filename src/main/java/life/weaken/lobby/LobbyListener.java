package life.weaken.lobby;

import life.weaken.lobby.LobbyPlugin;
import life.weaken.lobby.gui.GUI;
import life.weaken.lobby.gui.GUIManager;
import life.weaken.lobby.gui.guis.GuiSelectServerScreen;
import life.weaken.lobby.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collections;
import java.util.List;

public record LobbyListener(LobbyPlugin plugin, GUIManager guiManager, Location spawn) implements Listener {

    private static final ItemStack ELYTRA = new ItemBuilder(Material.ELYTRA)
            .setName("&bWings of God")
            .setUnbreakable(true)
            .create();

    private static final ItemStack FIREWORK = new ItemBuilder(Material.FIREWORK_ROCKET)
            .setName("&bMagic Firework &7(Right Click)")
            .setLore(Collections.singletonList("&7&oLegends say it never runs out!"))
            .create();

    private static final ItemStack COMPASS = new ItemBuilder(Material.COMPASS)
            .setName("&bServer Selector &7(Right Click)")
            .setLore(List.of("&7&oRight click to open the", "&7&oserver selection menu"))
            .create();

    private static final ItemStack PEARL = new ItemBuilder(Material.ENDER_PEARL)
            .setName("&bMagic Pearl &7(Right Click)")
            .setLore(Collections.singletonList("&7&oLegends say it never runs out!"))
            .create();

    @EventHandler
    private void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.getInventory().getItemInMainHand().getType() == Material.COMPASS) {
            new GuiSelectServerScreen(plugin, guiManager, player);
        } else if ((event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && event.getHand() == EquipmentSlot.HAND) {

            if (player.getInventory().getItemInMainHand().getType() == Material.FIREWORK_ROCKET) {
                if (player.isGliding() && player.getCooldown(Material.FIREWORK_ROCKET) < 1) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            player.getInventory().setItem(0, FIREWORK);
                            player.setCooldown(Material.FIREWORK_ROCKET, 20);
                        }
                    }.runTaskLater(plugin, 1);
                } else {
                    event.setCancelled(true);
                }
            } else if (player.getInventory().getItemInMainHand().getType() == Material.ENDER_PEARL) {
                if (player.getCooldown(Material.ENDER_PEARL) < 1) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            player.getInventory().setItem(8, PEARL);
                            player.setCooldown(Material.ENDER_PEARL, 20);
                        }
                    }.runTaskLater(plugin, 1);
                }
            } else {
                event.setCancelled(true);
            }

        }
    }

    @EventHandler
    private void onDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    private void onSwitchItemEvent(PlayerSwapHandItemsEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    private void onDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player player) {
            event.setCancelled(true);
            if (event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) initPlayer(player);
        }
    }

    @EventHandler
    private void onSpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.NATURAL)) event.setCancelled(true);
    }

    @EventHandler
    private void onJoin(PlayerJoinEvent event) {
        initPlayer(event.getPlayer());
    }

    @EventHandler
    private void onHunger(FoodLevelChangeEvent event) {
        event.getEntity().setFoodLevel(20);
    }

    //Initialize player
    @SuppressWarnings("ConstantConditions")
    private void initPlayer(Player player) {
        player.teleport(spawn);
        player.setGameMode(GameMode.ADVENTURE);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        player.setTotalExperience(1);

        PlayerInventory inventory = player.getInventory();
        inventory.clear();
        inventory.setChestplate(ELYTRA);
        inventory.setItem(0, FIREWORK);
        inventory.setItem(4, COMPASS);
        inventory.setItem(8, PEARL);
    }
}
