package life.weaken.lobby.gui;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GUIManager {

    public final Map<UUID, GUI> guiHashMap;

    public GUIManager() {
        guiHashMap = new HashMap<>();
    }

    public GUI getGUI(Player player) {
        return getGUI(player.getUniqueId());
    }

    public GUI getGUI(UUID uuid) {
        return guiHashMap.get(uuid);
    }

    public void addGUI(GUI gui) {
        guiHashMap.put(gui.player.getUniqueId(), gui);
    }

}
