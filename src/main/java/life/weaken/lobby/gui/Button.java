package life.weaken.lobby.gui;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface Button {
    void execute(InventoryClickEvent event);
}
