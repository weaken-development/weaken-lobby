package life.weaken.lobby.gui;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

public record GUIListener(GUIManager guiManager) implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        GUI gui = guiManager.getGUI((Player) e.getWhoClicked());
        if (gui != null) {
            gui.onInventoryClick(e, gui);
            if (gui.buttons.get(e.getRawSlot()) != null) {
                gui.buttons.get(e.getRawSlot()).execute(e);
            }
        }
        if (!e.getWhoClicked().getGameMode().equals(GameMode.CREATIVE)) e.setCancelled(true);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        GUI gui = guiManager.getGUI((Player) e.getPlayer());
        if (gui != null) {
            gui.onInventoryClose(e, gui);
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent e) {
        GUI gui = guiManager.getGUI((Player) e.getPlayer());
        if (gui != null) {
            gui.onInventoryOpen(e, gui);
        }
    }

    @EventHandler
    public void onInventoryDrop(PlayerDropItemEvent e) {
        GUI gui = guiManager.getGUI(e.getPlayer());
        if (gui != null) {
            gui.onItemDrop(e, gui);
        }
    }
}
