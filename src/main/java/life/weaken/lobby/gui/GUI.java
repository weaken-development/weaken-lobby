package life.weaken.lobby.gui;

import life.weaken.lobby.LobbyPlugin;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class GUI {

    private final LobbyPlugin plugin;
    private final GUIManager guiManager;

    public final Player player;
    protected final Inventory inventory;

    protected final ItemStack[] items;
    public final Map<Integer, Button> buttons = new HashMap<>();

    public GUI(LobbyPlugin plugin, GUIManager guiManager, Player player, int size, String title) {
        this.plugin = plugin;
        this.guiManager = guiManager;

        if(guiManager.getGUI(player) != null) {
            guiManager.guiHashMap.remove(player.getUniqueId());
        }
        inventory = Bukkit.createInventory(player, size, title);
        this.player = player;
        items = new ItemStack[size];
        guiManager.addGUI(this);
    }

    public void setButtons(int slot, Button button) {
        buttons.put(slot, button);
    }

    public void setButtons(int slot, ItemStack itemStack, Button button) {
        setButtons(slot, button);
        items[slot] = itemStack;
    }

    public void switchScreen(GUI gui) {
        guiManager.addGUI(gui);
    }

    public void update() {
        inventory.setContents(items);
    }

    public static ItemStack createItem(Material item, int count, String name, List<String> lore) {
        ItemStack itemStack = createItem(item, count, name);
        ItemMeta meta = itemStack.getItemMeta();
        assert meta != null;
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        return parseColor(itemStack);
    }

    public static ItemStack createItem(Material item, int count, String name) {
        ItemStack itemStack = new ItemStack(item, count);
        ItemMeta meta = itemStack.getItemMeta();
        assert meta != null;
        meta.setDisplayName(name);
        meta.setUnbreakable(true);
        itemStack.setItemMeta(meta);
        return parseColor(itemStack);
    }

    private static final Pattern hexPattern = Pattern.compile("<#([A-Fa-f0-9]){6}>");

    public static String applyColor(String message){
        Matcher matcher = hexPattern.matcher(message);
        while (matcher.find()) {
            final ChatColor hexColor = ChatColor.of(matcher.group().substring(1, matcher.group().length() - 1));
            final String before = message.substring(0, matcher.start());
            final String after = message.substring(matcher.end());
            message = before + hexColor + after;
            matcher = hexPattern.matcher(message);
        }
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static ItemStack parseColor(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        if(meta != null){
            List<String> lore = meta.getLore();
            if(lore != null) {
                for (int j = 0; j < lore.size(); j++) {
                    lore.set(j, applyColor(lore.get(j)));
                }
            }
            meta.setLore(lore);
            meta.setDisplayName(applyColor(meta.getDisplayName()));

            item.setItemMeta(meta);
        }
        return item;
    }


    public void onInventoryClick(InventoryClickEvent e, GUI gui){}

    public void onInventoryClose(InventoryCloseEvent e, GUI gui){
        guiManager.guiHashMap.remove(player.getUniqueId());
        Bukkit.getScheduler().runTaskLater(plugin, player::updateInventory, 1);
    }

    public void onInventoryOpen(InventoryOpenEvent e, GUI gui){}

    public void onItemDrop(PlayerDropItemEvent e, GUI gui){}
}
