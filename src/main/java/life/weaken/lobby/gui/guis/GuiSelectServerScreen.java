package life.weaken.lobby.gui.guis;

import life.weaken.core.bungeeapi.BungeeAPI;
import life.weaken.lobby.LobbyPlugin;
import life.weaken.lobby.gui.GUI;
import life.weaken.lobby.gui.GUIManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.List;

public class GuiSelectServerScreen extends GUI {

    private final BungeeAPI bungeeAPI;

    public GuiSelectServerScreen(LobbyPlugin plugin, GUIManager guiManager, Player player) {
        super(plugin, guiManager, player, 45, "Select Server");
        this.bungeeAPI = plugin.getCore().getBungeeAPI();
        setItems();
        player.openInventory(inventory);
    }

    private void setItems() {
        for(int i = 0; i < items.length; i++) {
            if(i < 10 || i > 34 || i % 9 == 0 || i % 9 == 8) {
                items[i] = createItem(Material.GRAY_STAINED_GLASS_PANE, 1, "&0");
            }
        }
        setButtons(20, GUI.createItem(Material.WHITE_BED, 1,
                "&bSMP",
                List.of("&7&oCreate team and domate the lands", "&7&oin a SMP like no other")),
                event -> bungeeAPI.connect(player, "WeaknSMP"));
        setButtons(24, GUI.createItem(Material.TNT, 1,
                        "&bBox",
                        List.of("&7&oJoin and fight with other players", "&7&oin a fast-paced box environment!")),
                event -> bungeeAPI.connect(player, "WeakenBox"));
        update();
    }

    @Override
    public void onInventoryClick(InventoryClickEvent e, GUI gui) {
        e.setCancelled(true);
    }
}
