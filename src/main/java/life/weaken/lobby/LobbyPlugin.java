package life.weaken.lobby;

import life.weaken.core.CorePlugin;
import life.weaken.core.utils.FutureUtils;
import life.weaken.lobby.cache.ServerDataCache;
import life.weaken.lobby.npc.LobbyNpcCommand;
import life.weaken.lobby.gui.GUIListener;
import life.weaken.lobby.gui.GUIManager;
import life.weaken.lobby.npc.NpcListener;
import life.weaken.lobby.npc.NpcManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.logging.Level;

public class LobbyPlugin extends JavaPlugin {

    private CorePlugin corePlugin;

    private List<String> serverList;
    private NpcManager npcManager;
    private GUIManager guiManager;
    private ServerDataCache dataCache;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void onEnable() {
        if(!getDataFolder().exists()) getDataFolder().mkdirs();
        saveDefaultConfig();

        grabDependency("Citizens");
        corePlugin = (CorePlugin) grabDependency("Weaken-Core");

        guiManager = new GUIManager();

        Future<List<String>> future =  corePlugin.getBungeeAPI().getServers();
        FutureUtils.waitForFuture(this, () -> {
            serverList = FutureUtils.get(future);

            Bukkit.getScheduler().runTask(this, () -> {
                dataCache = new ServerDataCache(this);
                npcManager = new NpcManager(this);
                registerCommands();
                Bukkit.getPluginManager().registerEvents(new NpcListener(this), this);
            });
        }, future);

        registerListeners();

        final World world = Bukkit.getWorld("world");
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            if (world == null) return;
            world.setTime(world.getTime() + 20 * 3);
        }, 0L, 1L);

    }

    public NpcManager getNpcManager() {
        return npcManager;
    }

    public CorePlugin getCore() {
        return corePlugin;
    }

    public List<String> getServerList() {
        if(serverList == null) return new ArrayList<>();
        return serverList;
    }

    public ServerDataCache getDataCache() {
        return dataCache;
    }

    @Override
    public void onDisable() {
        npcManager.shutdown();
        dataCache.shutdown();
    }


    @SuppressWarnings("ConstantConditions")
    private void registerCommands() {
        getCommand("lobbynpc").setExecutor(new LobbyNpcCommand(this));
    }
    private void registerListeners() {
        Location loc = new Location(Bukkit.getWorld("world"),
                getConfig().getInt("spawn-location.x") + 0.5,
                getConfig().getInt("spawn-location.y"),
                getConfig().getInt("spawn-location.z") + 0.5);
        loc.setYaw(getConfig().getInt("spawn-location.yaw"));
        loc.setPitch(getConfig().getInt("spawn-location.pitch"));

        getServer().getPluginManager().registerEvents(new LobbyListener(this, guiManager, loc),this);
        getServer().getPluginManager().registerEvents(new GUIListener(guiManager), this);
    }

    private Plugin grabDependency(String name) {
        Plugin plugin = Bukkit.getPluginManager().getPlugin(name);
        if(plugin == null || !plugin.isEnabled()) {
            getLogger().log(Level.SEVERE, "Could not find " + name + ", disabling...");
            getServer().getPluginManager().disablePlugin(this);
        }
        return plugin;
    }
}
